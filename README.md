# Auteur

Pheron Fabien

# Présentation 

Ce projet a pour but de valider ma licence professionnelle administration systèmes et réseaux.
D'autres ajouts sont à venir, notamment la gestion des erreurs (rescue/debug)
J'ai préféré configurer un compte sudo "sio" et procéder à l'installation de tous les programmes avec le compte( pour plus de sécurité et limiter l'utilisation du compte root).


## Installation et configuration

Avant de lancer le script, n'oublier surtout pas de modifier les variables, du fichier /default/default.yaml.
Choissons un mot de passe pour notre root.
```bash
[mariadb_root_password: 'sio'].
```
Il faut maintenant définir notre sudo.
Pour cela, il faudra créer un utilisateur( ou utiliser un user ayant les droits sudo)

Une fois celà fait, il faudra l'informer à ansible dans le fichier /tasks/main.yaml (remplacer sudo par l'utilisateur sudo)
```bash
 remote_user: sio
```

Pour lancer le playbook, exécuter la commande
```bash
ansible-playbook main.yaml --user=sio --extra-vars "ansible_sudo_pass=sio"
`

```

# À venir

-Ajout d'un message lors de l'étape "skipping"(vérification mot de passe root)
-Configuration d'un mode rescue, permettant de réinstaller mariadb, si le service mariadb ne parvient pas à se lancer.
-Ajout d'une fonctionnalité, permettant de créer un utilisateur et un mot de passe ( à définir seulement dans le dossier défault)


